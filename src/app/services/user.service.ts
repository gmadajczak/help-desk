import { Injectable } from '@angular/core';
import {User} from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User;

  get fullName(): string {
    return this.user ? `${this.user.name} ${this.user.lastname}` : '';
  }
  logout() {
    this.user = undefined;
  }
}
