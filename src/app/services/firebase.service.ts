import { Injectable } from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireAuth} from 'angularfire2/auth';
import {User} from '../interfaces/user';
import {UserService} from './user.service';
import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  messages$: Observable<any[]>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private afdb: AngularFireDatabase,
    private userService: UserService
  ) {
    this.messages$ = afdb.list('messages').valueChanges();
  }
  register(user: User): Promise<any> {
    return this.afAuth.auth.createUserAndRetrieveDataWithEmailAndPassword(user.email, user.password)
      .then((resp: any) => {
        const usersCollection = this.afs.collection<any>('users');
        user.uid = resp.user.uid;
        delete user.password;
        usersCollection.add(user);
      });
  }
  async login(email: string, password: string) {
    const uid: string = await this.afAuth.auth.signInWithEmailAndPassword(email, password).then((resp) => resp.user.uid);
    return uid;
  }

  userData(uid: string) {
    this.afs.collection('users', ref => ref.where('uid', '==', uid))
      .valueChanges().subscribe(
        (data: any) => {
          this.userService.user = data[0];
        }
      );
  }

  sendMessage(content: string) {
    const refs = this.afdb.list('messages');
    refs.push({ content, user: this.userService.fullName });
  }
}
