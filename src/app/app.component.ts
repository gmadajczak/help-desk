import { Component } from '@angular/core';
import { UserService } from './services/user.service';
import { User } from './interfaces/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  get user(): User { return this.userService.user; }
  constructor(private userService: UserService, private router: Router) {}
  logout() {
    this.userService.logout();
    this.router.navigate(['login']);
  }
}
