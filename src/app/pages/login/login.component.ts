import { Component } from '@angular/core';
import { LoginData } from '../../interfaces/login-data';
import {FirebaseService} from '../../services/firebase.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginData: LoginData = {
    email: '',
    password: '',
  };
  constructor(private firebase: FirebaseService, private router: Router) {}
  logIn() {
    this.firebase.login(this.loginData.email, this.loginData.password)
      .then((resp) => {
        this.firebase.userData(resp);
        this.router.navigate(['main']);
      });
  }
}
