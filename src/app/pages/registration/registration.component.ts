import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../interfaces/user';
import { FirebaseService } from '../../services/firebase.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  regForm: FormGroup;

  constructor(private fb: FormBuilder, private firebase: FirebaseService) {
    this.createForm();
  }
  createForm(): void {
    this.regForm = this.fb.group({
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordRepeat: ['', [Validators.required, Validators.minLength(8)]],
    });
  }
  saveUser(): void {
    const user: User = {
      name: this.regForm.get('name').value,
      lastname: this.regForm.get('lastname').value,
      email: this.regForm.get('email').value,
      password: this.regForm.get('password').value,
    };
    this.firebase.register(user)
      .then(() => {
        this.regForm.reset();
      });
  }
}
