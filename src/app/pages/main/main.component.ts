import { Component } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {Message} from '../../interfaces/message';
import {Observable} from 'rxjs';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  content: string;
  messages$: Observable<Message[]>;
  get fullname() { return this.userService.fullName; }
  constructor(private firebase: FirebaseService, private userService: UserService) {
    this.messages$ = this.firebase.messages$;
  }
  sendMessage() {
    this.firebase.sendMessage(this.content);
  }
}
